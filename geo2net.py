import sys
import numpy as np
from scipy.ndimage import distance_transform_edt
from skimage.segmentation import watershed
from skimage.future.graph import RAG,rag_boundary
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from copy import deepcopy
from scipy.spatial.distance import cosine,euclidean
from skimage.filters.rank import windowed_histogram as whist
from networkx import connected_components,compose,empty_graph
from skimage.morphology import convex_hull_image
from skimage.measure import regionprops
from skfuzzy.cluster import cmeans
import hdbscan

#sys.path.append('D:\Mes donnees\__WORK__\Developpement\pycharm-workspace\moringa')
sys.path.append('/Users/raffaele/python/moringa')
from sitsproc_c_modules.obiatools import zonalstats,OSTAT_MEAN,OSTAT_COUNT,OSTAT_SUM

def objectStats(seg,img,stats,nodata_value=-9999.0):
    """
    Computes statistics for objects based on a segmentation image and a reference image.
    :param seg:
    :param img:
    :param stats:
    :param nodata_value:
    :return:
    """
    if len(img.shape) == 2:
        img = np.array([img])
    if type(stats) == int:
        stats = [stats]
    zstat, stats_band_count = zonalstats(seg.astype(np.int32), img.astype(np.float32),
                                         np.arange(seg.shape[0]).astype(np.int32),
                                         np.arange(seg.shape[1]).astype(np.int32),
                                         np.array(stats).astype(np.int32),
                                         np.array([nodata_value] * img.shape[0]).astype(np.float32))
    return zstat

def objectLabelPropagation(map):
    """
    Produces a full map (intended for RAG computation) from an incomplete object layer.
    :param map:
    :param bv:
    :return:
    """
    df = distance_transform_edt(map==0)
    return watershed(df,map), df

def filterMap(map,labels):
    return np.multiply(map,np.isin(map,np.array(labels)))


def objectKMeans(seg,img,nc,nodata_value=-9999.0,return_cluster_map = False):
    """
    Performs K-Means clustering on object means from a reference image.
    :param seg:
    :param img:
    :param nc:
    :param nodata_value:
    :return:
    """
    zstat = objectStats(seg,img,OSTAT_MEAN,nodata_value)
    km = KMeans(n_clusters=nc).fit(np.array(zstat.values()))
    clust_dict = dict(zip(zstat.keys(),km.labels_+1))
    if not return_cluster_map:
        return clust_dict
    else:
        # Builds the class/cluster map
        lut = np.zeros(np.array(clust_dict.keys()).max() + 1)
        for k, v in clust_dict.items():
            lut[k] = v
        clust_map = lut[seg]
        return clust_dict, clust_map

def objectCMeans(seg,img,nc,pc=0,nodata_value=-9999.0):
    print("Running CMeans")
    zstat = objectStats(seg,img,OSTAT_MEAN,nodata_value)
    data = np.array(list(zstat.values())).swapaxes(0,1)
    cntr, u, u0, d, jm, p, fpc = cmeans(data,nc,2, error=0.005, maxiter=1000, init=None)

    clust_dict = {}
    fuzzym = u.swapaxes(0,1)
    return fuzzym
    """
    for i in range(len(zstat.keys())):
        th =np.percentile(fuzzym[i],pc)
        clust_dict[zstat.keys()[i]] = np.ndarray.flatten(np.argwhere(fuzzym[i]>=th))+1
    return clust_dict
    """

def objectHDBScan(seg,img):
    print("Running HDBScan")
    zstat = objectStats(seg,img,OSTAT_MEAN,nodata_value=-9999.0)
    #data = np.array(zstat.values()).swapaxes(0,1)
    data = np.array(zstat.values())



    clusterer = hdbscan.HDBSCAN(min_cluster_size=5, prediction_data=True)
    clusterer.fit(data)
    soft_clusters = hdbscan.all_points_membership_vectors(clusterer)
    return soft_clusters


def transitionProbabilities(seg,clust_dict):
    """
    Computes class/cluster transition probabilities in space (2st order neighborhood) at object level.
    :param seg:
    :param clust_dict:  for each segment, its cluster id
    :return:
    """
    # Builds the class/cluster map
    lut = np.zeros(np.array(clust_dict.keys()).max()+1)
    for k,v in clust_dict.items():
        lut[k] = v
    clust_map = lut[seg]
    # Computes the 8-directional shifts
    shifted_clust_maps = np.empty((8,seg.shape[0],seg.shape[1]))
    shifts = [(1,0),(1,1),(0,1),(-1,1),(-1,0),(-1,-1),(0,-1),(1,-1)]
    for i in range(8):
        shifted_clust_maps[i] = np.roll(clust_map,shifts[i],(0,1))
    # Prepare dictionary with transition probabilities
    clusters = np.unique(clust_dict.values())
    nc = len(clusters)

    tps = {x : {} for x in clusters}
    for k in clust_dict.keys():
        tps[clust_dict[k]][k] = np.zeros((nc,8))
    # Compute per-class/per-cluster transition probabilities as object statistics (sum/count)
    areas = objectStats(seg,seg,OSTAT_COUNT)
    i = 0
    for c in clusters:
        zstat = objectStats(seg,shifted_clust_maps==c,OSTAT_SUM)
        # for each segment
        for k in clust_dict.keys():
            # for each cluster, for each segment in that cluster, for each other cluster, tp in the 8 directions
            # (i.e., one array of lenght 8 for each cluster i)
            # NB: generally higher values when i==clust_dict[k] ---higher probability of adjacency within the same cluster
            tps[clust_dict[k]][k][i] = zstat[k]/areas[k]
        i += 1
    # tp partitioned for the k clusters  (one entry per cluster)
    return tps

def transitionProbabilities_v2(seg,clust_dict):
    """
    Computes class/cluster transition probabilities in space (2st order neighborhood) at object level.
    :param seg:
    :param clust_dict:  for each segment, its cluster id
    :return:
    """
    # Builds the class/cluster map
    lut = np.zeros(np.array(clust_dict.keys()).max()+1)
    for k,v in clust_dict.items():
        lut[k] = v
    clust_map = lut[seg]
    # Computes the 8-directional shifts
    shifted_clust_maps = np.empty((8,seg.shape[0],seg.shape[1]))
    shifts = [(1,0),(1,1),(0,1),(-1,1),(-1,0),(-1,-1),(0,-1),(1,-1)]
    for i in range(8):
        shifted_clust_maps[i] = np.roll(clust_map,shifts[i],(0,1))
    # Prepare dictionary with transition probabilities
    clusters = np.unique(clust_dict.values())
    nc = len(clusters)

    tps = {x : {} for x in clusters}
    tps_clust = {}
    areas_clust = {}
    for k in clust_dict.keys():
        tps[clust_dict[k]][k] = np.zeros((nc,8))
    for k in clusters:
        tps_clust[k]=np.zeros((nc,8))
        areas_clust[k] = np.zeros((nc,8))

    # Compute per-class/per-cluster transition probabilities as object statistics (sum/count)
    areas = objectStats(seg,seg,OSTAT_COUNT)
    i = 0
    for c in clusters:
        zstat = objectStats(seg,shifted_clust_maps==c,OSTAT_SUM)
        # for each segment
        for k in clust_dict.keys():
            # for each cluster, for each segment in that cluster, for each other cluster, tp in the 8 directions
            # (i.e., one array of lenght 8 for each cluster i)
            # NB: generally higher values when i==clust_dict[k] ---higher probability of adjacency within the same cluster
            tps[clust_dict[k]][k][i] = zstat[k]/areas[k]
            tps_clust[clust_dict[k]][i]+=zstat[k]
            areas_clust[clust_dict[k]][i]+=areas[k]

        i += 1
    for k in clusters:
        tps_clust[k]=tps_clust[k]/areas_clust[k]
    # tps: tp partitioned for the k clusters  (one entry per cluster)
    # tps_clust: probs cumulated for each cluster (for interlayer weights)
    return tps,tps_clust

def transitionProbabilities_hdbscan(seg,soft_clusters):

    """
    Computes class/cluster transition probabilities in space (2st order neighborhood) at object level.
    :param seg:
    :param soft_clusters:  for each segment, membership probability for each cluster
    :return:
    """
    # un cluster dict per ogni cluster, contenente probabilita di membership per ogni segmento
    probs_per_clust = {}
    for i in range(len(soft_clusters[1])):
        probs_per_clust[i]=np.zeros(len(soft_clusters) + 1)
    for i in range(len(soft_clusters)):
        for c in probs_per_clust.keys():
            probs_per_clust[c][i+1]=soft_clusters[i][c]

    #una clust map per ogni cluster, contenente la matrice dei segmenti con le probs di appartenenza al cluster
    clust_maps = {}
    for c in probs_per_clust:
        lut = np.array(probs_per_clust[c])
        clust_maps[c]=lut[seg]

    # uno shifted_clust_maps array per cluster (prob appartenenza vicini in ogni direzione)
    shifted_probs = {}
    for c in probs_per_clust.keys():
        # Computes the 8-directional shifts
        shifted_clust_maps = np.empty((8,seg.shape[0],seg.shape[1]))
        shifts = [(1,0),(1,1),(0,1),(-1,1),(-1,0),(-1,-1),(0,-1),(1,-1)]
        for i in range(8):
            shifted_clust_maps[i] = np.roll(clust_maps[c],shifts[i],(0,1))
        shifted_probs[c]=shifted_clust_maps

    # Prepare dictionary with transition probabilities
    nc = len(soft_clusters[1])
    thres = 1.0/nc
    clusters = np.array(range(nc))
    tps = {x : {} for x in range(1,len(soft_clusters)+1)}
    tps_clust = {}
    areas_clust = {}
    count = {}
    for i in range(len(soft_clusters)):
        #tps[i+1] = np.zeros((nc,8))
        tps[i+1] = np.zeros((nc, 8))
    for k in clusters:
        tps_clust[k]=np.zeros((nc,8))
        areas_clust[k] = np.zeros((nc,8))
        count[k]=0


    #print( "TRANSITION PROBABILITIES TO CHECK/MODIFY")
    # Compute per-class/per-cluster transition probabilities as object statistics (sum/count)
    areas = objectStats(seg,seg,OSTAT_COUNT)
    i = 0

    for c in clusters:
        zstat = objectStats(seg,shifted_probs[c],OSTAT_SUM)
        # for each segment
        for k in range(1,len(soft_clusters)+1):
            # for each cluster, for each segment in that cluster, for each other cluster, tp in the 8 directions
            # (i.e., one array of lenght 8 for each cluster i)
            # NB: generally higher values when i==clust_dict[k] ---higher probability of adjacency within the same cluster
            tps[k][i] = zstat[k]/areas[k]

            for c in clusters:
                #cluster context is built upon the nodes that belong to that cluster
                if soft_clusters[k-1][c]>thres:
                    count[c]+=1
                    tps_clust[c][i]+=zstat[k]
                    areas_clust[c][i]+=areas[k]
        i += 1
    for k in clusters:
        tps_clust[k]=tps_clust[k]/areas_clust[k]
    # tps: tp partitioned for the k clusters  (one entry per cluster)
    # tps_clust: probs cumulated for each cluster (for interlayer weights)
    return tps,tps_clust

def getInterlayerWeights(tps_clust):
    nc = len(tps_clust.keys())
    ws = np.zeros((nc,nc))
    for c in tps_clust.keys():
        for d in tps_clust.keys():
            ws[int(c)-1][int(d)-1]=1.0 - cosine(np.ndarray.flatten(tps_clust[c]),np.ndarray.flatten(tps_clust[d]))
    return ws

def reduceTP(tps, method='tfr'):
    """
    Create descriptors based on dimensionality reduction of the original transition probability (TP) matrices.
    Method TFR: see Scarpa et al. 2009
    :param tps:
    :param clusters:
    :param method:
    :return:
    """
    rtps = deepcopy(tps)
    rrtps = {}
    if method=='tfr':
        # Compute log-probabilities
        clusters = np.unique(rtps.keys())
        nc = len(clusters)
        n_pca_comp = []
        for c in range(nc):
            for k,v in rtps[clusters[c]].items():
                # to be rediscussed!
                eps = np.min(v[v.nonzero()])/10
                v[np.where(v == 0)] += eps
                v[np.where(v == 1)] -= eps
                for i in range(v.shape[0]):
                    if clusters[i] == clusters[c]:
                        v[i] = np.log(1-v[i])
                    else:
                        v[i] = np.log(np.divide(v[i],1-v[c]))
                rtps[clusters[c]][k] = v
            # Dimensionality reduction over directions (factor 8)...
            k = rtps[clusters[c]].keys()
            v = rtps[clusters[c]].values()
            rrtps[clusters[c]] = {x: [] for x in k}
            tq = np.empty((len(k),nc))
            for i in range(nc):
                q = np.array([x[i, :] for x in v])
                pca = PCA(1)
                tq[:,i] = pca.fit(q).transform(q).flatten()
            #and over clusters(90 % energy)
            pca = PCA(0.90,svd_solver='full')
            fq = pca.fit(tq).transform(tq)
            n_pca_comp.append(pca.n_components_)
            for t in range(len(k)):
                rrtps[clusters[c]][k[t]] = fq[t]

    # n_pca_comp: for each cluster, number of selected pca components
    # rrtps: for each cluster i, for each segment in that cluster, array of len=n_pca_comp[i]
    return rrtps, n_pca_comp


# distance weights
def generateClusterGraphs(seg,tps):
    clusters = np.unique(np.array(tps.keys()))
    distance_rags = {}
    context_rags = {}
    for c in clusters:
        ks = np.array(tps[c].keys())
        cseg, odf = objectLabelPropagation(np.multiply(seg,np.isin(seg,ks)))
        distance_rags[c] = rag_boundary(cseg, odf)
        context_rags[c] = distance_rags[c].copy()
        for u,v,d in context_rags[c].edges(data=True):
            d.clear()
            d['weight'] = np.square(tps[c][u]-tps[c][v]).mean()
    return distance_rags, context_rags


# versione con pesi cosine similarity su flattened 8-directions matrix
def generateClusterGraphs_cosine(seg,tps_original):
    clusters = np.unique(np.array(tps_original.keys()))
    distance_rags = {}
    context_rags = {}
    for c in clusters:
        ks = np.array(tps_original[c].keys())
        cseg, odf = objectLabelPropagation(np.multiply(seg,np.isin(seg,ks)))
        distance_rags[c] = rag_boundary(cseg, odf)
        context_rags[c] = distance_rags[c].copy()
        for u,v,d in context_rags[c].edges(data=True):
            d.clear()
            d['weight'] = 1.0-cosine(np.ndarray.flatten(tps_original[c][u]),np.ndarray.flatten(tps_original[c][v]))
    return distance_rags, context_rags

# versione con pesi cosine similarity su flattened 8-directions matrix
def generateClusterGraphs_cosine_DB(seg,soft_clusters,tps_original, membership_weights=False):
    NC = soft_clusters.shape[1]
    distance_rags = {}
    context_rags = {}
    tsc = soft_clusters * (soft_clusters > (1.0/NC))
    srt = np.argsort(soft_clusters)
    #for c in range(0,soft_clusters.shape[1]):
    for c in range(0, NC):
        clust_id = c+1
        distance_rags[clust_id] = empty_graph(0) #rag_boundary(cseg, odf)
        context_rags[clust_id] = empty_graph(0) #distance_rags[c].copy()
        ks = np.array([])
        for idx in range(NC-1,-1,-1):
            obj_in_c = np.intersect1d(np.where(srt[:,idx]==c)[0],np.where(tsc[:,c]>0)[0])+1
            ks = np.append(ks,obj_in_c)
            cseg, odf = objectLabelPropagation(np.multiply(seg,np.isin(seg,ks)))
            gD = rag_boundary(cseg,odf)
            gC = gD.copy()
            for u,v,d in gC.edges(data=True):
                d.clear()
                d['weight'] = 1.0-cosine(np.ndarray.flatten(tps_original[u]),np.ndarray.flatten(tps_original[v]))
                if membership_weights:
                    d['weight'] *= soft_clusters[u - 1][c] * soft_clusters[v - 1][c]
            distance_rags[clust_id] = compose(gD, distance_rags[clust_id])
            context_rags[clust_id] = compose(gC, context_rags[clust_id])
    return distance_rags, context_rags



def tfidfs(seg,clust_dict,k_num):
    """
    Computes tf/idf-like scores considering segments as documents, and adjacencies as words
    :param seg:
    :param clust_dict:  for each segment, its cluster id
    :return:
    """
    # Builds the class/cluster map
    lut = np.zeros(np.array(clust_dict.keys()).max()+1)
    for k,v in clust_dict.items():
        lut[k] = v
    clust_map = lut[seg]
    # Computes the 8-directional shifts
    shifted_clust_maps = np.empty((8,seg.shape[0],seg.shape[1]))
    shifts = [(1,0),(1,1),(0,1),(-1,1),(-1,0),(-1,-1),(0,-1),(1,-1)]
    for i in range(8):
        shifted_clust_maps[i] = np.roll(clust_map,shifts[i],(0,1))
    # Prepare dictionary with transition probabilities
    clusters = np.unique(clust_dict.values())
    nc = len(clusters)

    tfidf = {x : {} for x in clusters}
    for k in clust_dict.keys():
        tfidf[clust_dict[k]][k] = np.zeros((nc,8))
    # Compute per-class/per-cluster transition probabilities as object statistics (sum/count)
    areas = objectStats(seg,seg,OSTAT_COUNT)
    i = 0
    for c in clusters:
        zstat = objectStats(seg,shifted_clust_maps==c,OSTAT_SUM)
        # for each segment
        for k in clust_dict.keys():
            # for each cluster, for each segment in that cluster, for each other cluster, tp in the 8 directions
            # (i.e., one array of lenght 8 for each cluster i)
            # NB: generally higher values when i==clust_dict[k] ---higher probability of adjacency within the same cluster
            tfidf[clust_dict[k]][k][i] = zstat[k]
        i += 1
    # tp partitioned for the k clusters  (one entry per cluster)

    # document frequency array
    dfs = np.empty(k_num)
    #compute tf
    for k in clust_dict.keys():
        tf = np.sum(tfidf[clust_dict[k]][k], axis=1)
        tfidf[clust_dict[k]][k] = tf/sum(tf)
        for i in range(0,len(tf)):
            if tfidf[clust_dict[k]][k][i]>0:
                dfs[i]+=1
    #idf array
    idf = np.empty(k_num)
    for i in range(0,len(dfs)):
        idf[i]=np.log10(len(clust_dict.keys())/dfs[i])
    for k in clust_dict.keys():
        tfidf[clust_dict[k]][k] = tfidf[clust_dict[k]][k]*idf
    return tfidf

# Cosine similarity on tf-idf-like vectors (i.e., to use with tfidfs method)
def generateClusterGraphs_tfidf(seg,tfidf):
    clusters = np.unique(np.array(tfidf.keys()))
    distance_rags = {}
    context_rags = {}

    # for each cluster, for each segment in that cluster, for each other cluster, tp in the 8 directions
    # (i.e., one array of lenght 8 for each cluster i)
    # NB: generally higher values when i==clust_dict[k] ---higher probability of adjacency within the same cluster
    for c in clusters:
        ks = np.array(tfidf[c].keys())
        cseg, odf = objectLabelPropagation(np.multiply(seg,np.isin(seg,ks)))
        distance_rags[c] = rag_boundary(cseg, odf)
        context_rags[c] = distance_rags[c].copy()
        for u,v,d in context_rags[c].edges(data=True):
            d.clear()
            d['weight'] = 1.0-cosine(tfidf[c][u],tfidf[c][v])
    return distance_rags, context_rags

def thresholdGraph(gr, th, dir='ge', key='weight'):
    ggr = gr.copy()
    for u,v,d in gr.edges(data=True):
        if dir=='ge' and d[key] >= th:
            ggr.remove_edge(u, v)
        elif dir=='le' and d[key] <= th:
            ggr.remove_edge(u, v)
    return ggr

def getPercentile(gr,pc,key='weight'):
    w = []
    for u,v,d in gr.edges(data=True):
        w.append(d[key])
    w = np.array(w)
    return np.percentile(w,pc)

def thresholdAllGraphs(G,pc,dir='le',key='weight'):
    Gout = {}
    for c in G.keys():
        th = getPercentile(G[c],pc,key)
        Gout[c] = thresholdGraph(G[c],th,dir,key)
    return Gout


def full_rag(seg,weight_mode='default'):
    g = RAG(seg)
    if weight_mode=='default':
        return g
    elif weight_mode=='barycenter_distance':
        rp = regionprops(seg)
        for e in g.edges:
            g.edges[e]['weight'] = euclidean(rp[e[0]-1].centroid,rp[e[1]-1].centroid)
        return g



def histogramBasedContextDescriptors(cm,wsize,return_pca=False):
    wh = whist(cm.astype(np.uint8), np.ones((wsize, wsize)))
    wh = wh[:,:,1:]
    if return_pca:
        pca = PCA(n_components=3)
        rwh = np.reshape(wh, (wh.shape[0] * wh.shape[1], wh.shape[2]))
        prwh = pca.fit_transform(rwh)
        pwh = np.reshape(prwh, (wh.shape[0], wh.shape[1], 3))
        return wh, pwh
    else:
        return wh

def objectDiscreteContextDescriptors(seg,clust_dict,wsize):
    lut = np.zeros(np.array(clust_dict.keys()).max() + 1)
    for k, v in clust_dict.items():
        lut[k] = v
    cm = lut[seg]
    wh = histogramBasedContextDescriptors(cm,wsize)
    mns = objectStats(seg,np.moveaxis(wh,2,0).copy(),OSTAT_MEAN)
    # Prepare dictionary with transition probabilities
    clusters = np.unique(clust_dict.values())
    dcd = {x: {} for x in clusters}
    for k in mns.keys():
        dcd[clust_dict[k]][k] = mns[k].astype(np.float)
    return dcd

def linksBySpatialConsensus(cluster_graphs,seg):
    from matplotlib.pyplot import imshow,pause
    clist = cluster_graphs.keys()
    links = []
    ch = {}
    for c in clist:
        ch[c] = []
    sg = {}
    """
    for c in clist:
        ch.append([])
        print 'Computing convex hull for each connected component of cluster ' + str(c)
        sg.append([cluster_graphs[c].subgraph(q) for q in connected_components(cluster_graphs[c])])
        for g in sg[-1]:
            lab = list(g.nodes)
            ch[-1].append(convex_hull_image(np.isin(seg,lab).astype(np.uint8)))

    for i in range(len(ch)-1):
        for j in range(i,len(ch)):
            for u in ch[i]:
                for v in ch[j]:
                    imshow(u.astype(np.uint8)+v.astype(np.uint8))
                    pause(1)
    """
    for c in clist:
        lab = list(cluster_graphs[c].nodes)
        sg[c] = [cluster_graphs[c].subgraph(q) for q in connected_components(cluster_graphs[c])]
        for g in sg[c]:
            lab = list(g.nodes)
            ch[c].append(distance_transform_edt(1 - np.isin(seg, lab).astype(np.uint8),sampling=10))
        #imshow(ch[-1])
        #pause(1)

    """
    for i in clist[:-1]:
        for j in clist[1:]:
            for u in sg[i]:
                for v in sg[j]:
                    lab = list(u.nodes)
    """


    return ch

