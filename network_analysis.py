import os
import networkx as nx
import numpy as np
import glob


def readNet(input_path):
    layer_graphs = {}
    intra_w = []
    inter_w = []
    node_layers = {}
    interlayer = {}


    f = open(input_path, 'r')
    for line in f:
        vals = line.split(" ")
        l_u = int(vals[0].strip())
        u = int(vals[1].strip())
        l_v = int(vals[2].strip())
        v = int(vals[3].strip())
        w = float(vals[4].strip())

        if l_u not in layer_graphs:
            layer_graphs[l_u] = nx.Graph()
        if l_v not in layer_graphs:
            layer_graphs[l_v] = nx.Graph()
        if l_u == l_v:
            layer_graphs[l_u].add_edge(u, v)
            intra_w.append(w)
        else:
            inter_w.append(w)
            if u not in interlayer:
                interlayer[u]=set()
            if v not in interlayer:
                interlayer[v]=set()
            interlayer[u].add(v)
            interlayer[v].add(u)
        if u not in node_layers:
            node_layers[u] = set()
        if v not in node_layers:
            node_layers[v] = set()
        node_layers[u].add(l_u)
        node_layers[v].add(l_v)
    return layer_graphs,node_layers,interlayer



def ml_stats(input_path):
    layer_graphs = {}
    nodes = {}
    intra_w = []
    inter_w = []
    node_layers = {}

    f = open(input_path, 'r')
    for line in f:
        vals = line.split(" ")
        u = int(vals[1].strip())
        v = int(vals[3].strip())
        if u not in nodes:
            nodes[u]=np.zeros(2)
        if v not in nodes:
            nodes[v]=np.zeros(2)


    f = open(input_path, 'r')
    for line in f:
        vals = line.split(" ")
        l_u = int(vals[0].strip())
        u = int(vals[1].strip())
        l_v = int(vals[2].strip())
        v = int(vals[3].strip())
        w = float(vals[4].strip())


        if l_u not in layer_graphs:
            layer_graphs[l_u] = nx.Graph()
        if l_v not in layer_graphs:
            layer_graphs[l_v] = nx.Graph()
        if l_u==l_v:
            layer_graphs[l_u].add_edge(u, v)
            intra_w.append(w)
            nodes[u][0]+=1
            nodes[v][0]+= 1
        else:
            inter_w.append(w)
            nodes[u][1] += 1
            nodes[v][1] += 1
        if u not in node_layers:
            node_layers[u] = set()
        if v not in node_layers:
            node_layers[v] = set()
        node_layers[u].add(l_u)
        node_layers[v].add(l_v)

    print("Graph loaded, calculating statistics")


    print "#nodes: ", len(node_layers)
    print "#intralayer edges", len(intra_w)
    print "avg intra weight", np.mean(intra_w)
    print "#interlayer edges", len(inter_w)
    print "avg inter weight", np.mean(inter_w)
    print "#total edges",   len(intra_w)+len(inter_w)

    tot_deg = 0.0
    intra_deg = 0.0
    inter_deg = 0.0
    for u in nodes:
        intra_deg+=nodes[u][0]
        tot_deg+=nodes[u][0]
        inter_deg+=nodes[u][1]
        tot_deg += nodes[u][1]

    print "Average total degree", tot_deg/len(node_layers)
    print "Average intra degree", intra_deg/ len(node_layers)
    print "Average inter degree", inter_deg/ len(node_layers)



    ed = 0
    den = 0.0

    node_perl = []

    for l in layer_graphs:
        ed += len(layer_graphs[l].edges())
        nl = len(layer_graphs[l].nodes())
        node_perl.append(nl)
        den += nl * (nl - 1)
        # print("CC: ",l,nx.number_connected_components(graph[l]))
        # ccs = nx.connected_components(graph[l])
        # lens = []
        # for c in ccs:
        #    lens.append(len(c))
        # print("CC lengths: ", sorted(lens))

    print "Average nodes per layer",np.mean(node_perl)," std ",np.std(node_perl)
    density = (2 * ed) / (den)
    print "Total Intralayer Density: ", density

    tot_density = float(2*(len(intra_w)+len(inter_w)))/float(den)

    print "Total Global Density: ", tot_density




    ls = []
    for n in node_layers:
        ls.append(len(node_layers[n]))
    alayer = np.mean(ls)
    print("ALayer: ", alayer)



    print input_path,len(node_layers),len(intra_w), np.mean(intra_w),len(inter_w), np.mean(inter_w),   len(intra_w)+len(inter_w), tot_deg/len(node_layers), intra_deg/ len(node_layers), inter_deg/ len(node_layers), density,  tot_density, alayer

if __name__=='__main__':

    os.chdir("D:\\Mes Donnees\\Amoris\\geo2net-master\\geo2net-master\\ml_nets\\")

    files = glob.glob("*.ncol")
    for f in files:
        ml_stats(f)

