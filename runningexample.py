from geo2net import *
from matplotlib.pyplot import figure,imshow,plot,hist,show as pltshow, draw as pltdraw
from skimage.future.graph import show_rag
from skimage.exposure import rescale_intensity
from osgeo import gdal
import numpy as np
import networkx as nx
import os
from utilities import multilayer_format_geo2net

# Infomap binary
infomap = './infomap/Infomap.exe'
fld = './testdata'

# ndvi anno
ds = gdal.Open(os.path.join(fld,'koumbia_ts-ndvi.tif'))
img = ds.ReadAsArray()
ds = None
# seg singola immagine
segfile = os.path.join(fld,'koumbia_seg.tif')
ds = gdal.Open(segfile)
seg = ds.ReadAsArray()
ds = None
# immagine pulita agosto
ds = gdal.Open(os.path.join(fld,'koumbia.tif'))
rgb = ds.ReadAsArray()
ds = None

""""
# ndvi anno
ds = gdal.Open('./testdata/koumbia_ts-ndvi.tif')
print(ds)
img = ds.ReadAsArray()
ds = None
# seg singola immagine
segfile = './testdata/koumbia_seg.tif'
ds = gdal.Open(segfile)
seg = ds.ReadAsArray()
ds = None
# immagine pulita agosto
ds = gdal.Open('./testdata/koumbia.tif')
rgb = ds.ReadAsArray()
ds = None
"""

# Configuration
# Number k of initial clusters
k = 10

# select the weighting scheme
weighting_schemes = {'pca': 0,'flat' : 1,'tfidf' : 2}
weights = 'flat'

# select the context descriptor framework
context_schemes = {'tps' : 0, 'dcd' : 1}
context = 'tps'
dcd_windows_size = 100

# percentile for graph thresholding (0 for no thresholding)
pc = 30

# Output configuration
# base folder for experiment
bfld = '.'
# show cluster graphs (context)
show = False
# write distance graphs to disk
print_gd = False
# write context graphs to disk
print_gc = True
# write interlayer edges (implicitly builds the multilayer network file .ncol if context graphs are written)
interlayer = True
# interlayer edges onlu between nodes referring to same segment
just_coupling = False
# if True, each interlayer edge has weight w_inter. Otherwise, cosine similarity is used (geo2net.getInterlayerWights)
fixed_weights = True
w_inter = 0.5
#intralayer weights based on cluster memberships
membership_weights=False

# run community detection (silently builds the output segmentation image)
run_cd = False
#print multilayer network file for infomap
print_mlnet=True

# START process

base = np.empty((rgb.shape[1],rgb.shape[2], 3))
for i in range(3):
    base[:, :, i] = rescale_intensity(rgb[i], tuple(np.percentile(rgb[i], (2, 98))))

fld_out = bfld + os.sep + 'nets_K' + str(k) + '_' + weights + '_' + str(pc) + '_' + context

if not fixed_weights:
    fld_out+='_ilw'

if membership_weights:
    fld_out += '_mw'

if just_coupling:
    fld_out += '_coup'


fld_out +=os.sep


if context == 'dcd':
    fld_out += str(dcd_windows_size)

if (print_gc or print_gd or interlayer) and not os.path.exists(fld_out):
    os.mkdir(fld_out)


# per ogni timestamp (ndvi) , per ogni oggetto (segmento) = media
# kmeans su tutta la serie temporale (array medie ndvi  descrittore di ogni segmento)
#km = objectKMeans(seg,img,k)

# cmeans
#pc_cmeans = 50
db = objectCMeans(seg,img,k)

#db = objectHDBScan(seg,img)
#trans = transitionProbabilities_hdbscan(seg,db)
#exit()
# transition probability matrix (size k*8 for each segment of each cluster)
#tps = transitionProbabilities(seg, km)

#tps,tps_clust = transitionProbabilities_v2(seg, km)
tps,tps_clust = transitionProbabilities_hdbscan(seg,db)
inter_ws = getInterlayerWeights(tps_clust)
#print(inter_ws)

if weighting_schemes[weights]==0:
    # Versione originale (matrice tp ridotta con PCA)
    rtps, c = reduceTP(tps)
    Gd, Gc = generateClusterGraphs(seg, rtps)
elif weighting_schemes[weights]==1:
    # V2: Cosine similarity su matrice flattened delle tp originale (i.e., senza reduction)
    #Gd, Gc = generateClusterGraphs_cosine(seg, tps)
    if membership_weights:
        Gd, Gc = generateClusterGraphs_cosine_DB(seg, db, tps, membership_weights=True)
    else:
        Gd, Gc = generateClusterGraphs_cosine_DB(seg, db, tps)
elif weighting_schemes[weights]==2:
    # V3: Cosine similarity su score tf-idf
    print("DEPRECATED")
    #Gd, Gc = generateClusterGraphs_tfidf(seg,tfidfs(seg, km,k))

if pc > 0:
    Gct = thresholdAllGraphs(Gc,pc)
else:
    Gct = Gc

if interlayer:
    f = open(fld_out + 'interlayer_edges.ncol','w')
    if just_coupling:
        for g in Gc.keys():
            for u in Gc[g].nodes:
                for h in Gc.keys():
                    if h!=g:
                        for v in Gc[h].nodes:
                            if u==v:
                                w = inter_ws[int(h) - 1][int(g) - 1]
                                f.write("%s %s %s %s %f\n" % (u, g, v, h, w))
    else:
        full_rag = full_rag(seg)
        for g in Gc.keys():
            for u in Gc[g].nodes:
                for h in Gc.keys():
                    if h!=g:
                        for v in Gc[h].nodes:
                            if full_rag.has_edge(u,v):
                                w = inter_ws[int(h)-1][int(g)-1]
                                f.write("%s %s %s %s %f\n" % (u,g,v,h,w))
    f.close()

if print_gd:
    for g in Gd.keys():
        nx.write_edgelist(Gd[g], fld_out + "distance_rag_cluster_%d.ncol" % g)

if print_gc:
    for g in Gct.keys():
        nx.write_edgelist(Gct[g], fld_out + "context_rag_cluster_%d_%s_%d.ncol" % (g,weights,pc))

if show:
    for k in Gct.keys():
        show_rag(filterMap(seg,tps[k].keys()), Gct[k], base,edge_cmap='Spectral')
        pltdraw()
    pltshow()

if print_mlnet:
    if fixed_weights:
        multilayer_format_geo2net(fld_out,"mlnet_K%d_cmeans.ncol" % k)
    else:
        multilayer_format_geo2net(fld_out, "mlnet_K%d_cmeans.ncol" % k,weighted_input=True)
