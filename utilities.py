#!/usr/bin/env python
# -*- coding: utf-8 -*-

import glob
import os
from os.path import basename
import random



# writes a single multilayer ncol file starting from the separate ones produced with runningexample.py
# (i.e., 1 ncol per layer + 1 ncol for the interlayer_edges)
#
# Syntax to run infomap on this file:
#  ./Infomap -i'multilayer' <ml ncol file>  <output directory>   --clu --tree --bftree
def multilayer_format_geo2net(net_dir,outname,w_inter=0.5,wheuristic=True,weighted_input=False):
    os.chdir(net_dir)

    layer_prefix = "context_rag*.ncol"
    interlayer = "interlayer_edges.ncol"



    sum_w = 0.0
    c = 0

    fout = open(outname, 'w')
    #fout.write("# A network in a general multiplex format \n# layer node layer node [weight]\n")
    layers = glob.glob(layer_prefix)
    for l in layers:
        layer_num = str(int(basename(l).split('_')[3]))
        f = open(l, 'r')
        for line in f:
            vals = line.split(' ')
            w = float(vals[-1][:-2])
            fout.write("%s %s %s %s %s\n" % (layer_num, vals[0], layer_num, vals[1], w))
            sum_w+=w
            c+=1
        f.close()

    if wheuristic:
        w_inter=sum_w/(2*c)
        print(w_inter)

    f = open(interlayer,'r')
    for line in f:
        vals = line.split(' ')
        if weighted_input:
            fout.write("%s %s %s %s %s" % (vals[1], vals[0], vals[3], vals[2], vals[4]))
        else:
            fout.write("%s %s %s %s %f \n" % (vals[1],vals[0],vals[3].strip(),vals[2],w_inter))
    fout.close()


def flatten_network(input_path,output_path,out_sl):
    fout = open(output_path,'w')
    fsl = open(out_sl,'w')
    f = open(input_path, 'r')
    for line in f:
        vals = line.split(" ")
        u = int(vals[1].strip())
        v = int(vals[3].strip())
        w = float(vals[4].strip())
        fout.write("%d %d %d %d %f\n" % (1,u,1,v,w))
        fsl.write("%d %d %f\n" % (u,v,w))
    fout.close()
    fsl.close()


# writes a qml style file starting from a infomap clustering solution in .clu format
def qml_from_clu(clu_file, outname):
    fout = open(outname, 'w')
    coms = set()
    f = open(clu_file, 'r')
    for line in f:
        if not line.startswith("#"):
            vals = line.split(' ')
            com = int(vals[1])
            coms.add(int(com))

    print("Number of communities: ", len(coms))
    number_of_colors = len(coms)

    colors = ["#" + ''.join([random.choice('0123456789ABCDEF') for j in range(6)]) for i in range(number_of_colors)]

    fout.write("<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>\n")
    fout.write(r'<qgis version="2.16.1-Nødebo" minimumScale="100000" maximumScale="1e+08" hasScaleBasedVisibilityFlag="0"> <pipe> <rasterrenderer opacity="1" alphaBand="-1" band="1" type="paletted"> <rasterTransparency/> <colorPalette><!--<paletteEntry value="0" color="#ffffff" label="0"/> -->')
    fout.write('\n')

    f = open(clu_file, 'r')
    for line in f:
        if not line.startswith("#"):
            vals = line.split(' ')

            com = int(vals[1])
            col = colors[com-1]

            fout.write("<paletteEntry value=\"%s\" color=\"%s\" label=\"Community %d\"/>\n" % (vals[0],col,com) )

    fout.write(r'</colorPalette></rasterrenderer><brightnesscontrast brightness="0" contrast="0"/><huesaturation colorizeGreen="128" colorizeOn="0" colorizeRed="255" colorizeBlue="128" grayscaleMode="0" saturation="0" colorizeStrength="100"/><rasterresampler maxOversampling="2"/></pipe><blendMode>0</blendMode></qgis>')
    fout.close()

if __name__=='__main__':
    #multilayer_format_geo2net('nets_K10_flat_0_tps_ilweights','mlnet_K10_cmeans_ilweights.ncol',weighted_input=True)
    qml_from_clu(".\\output\\infomap\\mlnet_K10_cmeans_pc50_sl.clu","mlnet_K10_cmeans_pc50_sl.qml")
    #os.chdir("D:\\Mes Donnees\\Amoris\\geo2net-master\\geo2net-master\\ml_nets\\")
    #flatten_network("mlnet_K10_cmeans_pc50.ncol","mlnet_K10_cmeans_pc50_flattened.ncol","mlnet_K10_cmeans_pc50_sl.ncol")